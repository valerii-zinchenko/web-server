// This file is distributed under MIT license

var host = '0.0.0.0';
var port = 8080;

var http = require('http');
var zlib = require('zlib');
var fs = require('fs');
var path = require('path');
var url = require('url');

var mimeTypes = {
	'': 'text/plain',
	'.txt': 'text/plain',
	'.html': 'text/html',
	'.manifest': 'text/cache-manifest',
	'.css': 'text/css',
	'.js': 'application/javascript',
	'.woff': 'application/font-woff',
	'.woff2': 'application/font-woff2',
	'.svg': 'image/svg+xml',
	'.jpg': 'image/jpg',
	'.png': 'image/png',
	'.gif': 'image/gif',
	'.ico': 'image/x-icon',
	'.mp4': 'video/mp4',
	'.ogg': 'video/ogg',
	'.webm': 'video/webm',
	'.vtt': 'text/vtt'
};

function getFile(path, rs, mimeType) {
	fs.readFile(path, function(err, contents) {
		if (err) {
			rs.writeHead(500);
			rs.end();
		} else {
			rs.writeHead(200, {
				'Content-Type': mimeType,
				'Content-Length': contents.length
			});
			rs.end(contents);
		}
	});
}

http.createServer(function(rq, rs) {
	var resource = url.parse(rq.url).pathname;
	var filename = resource === '/' ? '/index.html' : resource;
	var ext = path.extname(filename);
	var localPath = __dirname + filename;

	var mimeType = mimeTypes[ext];

	if (mimeType) {
		fs.exists(localPath, function(exists){
			if (exists) {
				console.log('Serving file: ' + localPath );
				getFile(localPath, rs, mimeType || ext);
			} else {
				console.log('File not found: ' + localPath);
				rs.writeHead(404);
				rs.end();
			}
		});
	} else {
		console.log('Unknown file extensions: ' + ext);
		rs.writeHead(404);
		rs.end();
	}
}).listen(port, host);

console.log('Server running at http://' + host + ':' + port + '/');
